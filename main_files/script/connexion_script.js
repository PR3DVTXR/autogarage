function show_pwd() {
    if (document.getElementById("btn_show_pwd").innerHTML == "visibility_off") {
        document.getElementById("btn_show_pwd").innerHTML = "visibility";
        document.getElementById("pwd__input-id").type = "password";
    }
    else {
        document.getElementById("btn_show_pwd").innerHTML = "visibility_off"
        document.getElementById("pwd__input-id").type = "text";
    }
}
